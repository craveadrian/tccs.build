<div id="content">
	<div class="row">
		<h1>CONSTRUCTION PHOTOS</h1>
		<div class="inner-gallery">
			<div id="gall1" class="gallery-container">
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "construction") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/construction/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/construction/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
		</div>
	</div>
</div>
<footer>
	<div id="footer">
		<a href="<?php echo URL ?>contact#content"><h1>CONTACT US</h1>
