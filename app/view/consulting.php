<div id="mission2Section">
  <div class="row">
    <div class="ms2Left inb">
      <section>
        <h1>OUR MISSION</h1>
        <p>Our mission is to partner with customers to provide a consultative sales approach with both products and solutions. Our goal is to put the customers first by forming a partnership and delivering the best, most cost-effective solutions possible. Shaun Thomas leads our Consulting team with 15 years of Construction and Consulting experience. We focus our Consulting efforts in the healthcare field and are experienced in working with healthcare systems as a strategic vendor. Working with Supply Chain, Biomed, Nursing, Environmental Services, Surgical Services, Pharmacy, Equipment Planners, Architects, General Contractors. and the Sourcing Team to become your preferred vendor will be our goal.</p>
        <a href="<?php echo URL?>contact#content" class="btn">GET STARTED</a>
      </section>
    </div>
    <div class="ms2Right inb">
      <img src="public/images/content/consulting/img1.jpg" alt="storage">
    </div>
  </div>
</div>
<div id="consultingSvcSection">
  <h1>OUR SERVICES</h1>
  <div class="row">
    <div class="flex-boxes">
      <dl>
        <dt> <img src="public/images/content/consulting/svc1.jpg" alt="Consulting Services Image"> </dt>
        <dd>SPACE - EQUIPMENT <br> PLANNING</dd>
      </dl>
      <dl>
        <dt> <img src="public/images/content/consulting/svc2.jpg" alt="Consulting Services Image"> </dt>
        <dd>STRATEGIC SOURCING <br> CONSULTATION</dd>
      </dl>
      <dl>
        <dt> <img src="public/images/content/consulting/svc3.jpg" alt="Consulting Services Image"> </dt>
        <dd>PROJECT <br> MANAGEMENT</dd>
      </dl>
      <dl>
        <dt> <img src="public/images/content/consulting/svc4.jpg" alt="Consulting Services Image"> </dt>
        <dd>STORAGE - WAREHOUSING - <br>ASSEMBLY - LOGISTICS</dd>
      </dl>
      <dl>
        <dt> <img src="public/images/content/consulting/svc5.jpg" alt="Consulting Services Image"> </dt>
        <dd>INSTALLATION - <br>INTEGRATION - DEPLOYMENT</dd>
      </dl>
      <dl>
        <dt> <img src="public/images/content/consulting/svc6.jpg" alt="Consulting Services Image"> </dt>
        <dd>USER DEMOS - <br>TRAINING</dd>
      </dl>
      <dl>
        <dt> <img src="public/images/content/consulting/svc-placeholder.jpg" alt="Consulting Services Image"> </dt>
        <dd>WARE HOUSING & <br>RELOCATION SERVICES</dd>
      </dl>
    </div>
  </div>
</div>
<div id="prodSection">
  <div class="row">
    <h1>PRODUCTS OFFERED</h1>
    <ul>
      <li><p>Cart Covers</p></li>
      <li><p>Pharmacy Fixtures</p></li>
      <li><p>Custom Stainless-Steel Work</p></li>
      <li><p>MRI Cabinets and Surgical Cabinets</p></li>
      <li><p>MRI Cabinets and Surgical Cabinets</p></li>
      <li><p>Code Carts</p></li>
      <li><p>Procedure Carts</p></li>
      <li><p>Case Carts</p></li>
      <li><p>Linen Carts</p></li>
      <li><p>O2 Cylinder Carts</p></li>
      <li><p>Modular Case Goods</p></li>
      <li><p>Wall Mounts</p></li>
      <li><p>Wire Shelving</p></li>
      <li><p>High Density Shelving</p></li>
      <li><p>Plastic Bins & Systems</p></li>
      <li><p>Lockers (Metal & Polymer)</p></li>
      <li><p>OR Cabinets</p></li>
      <li><p>Storage/Material Handling</p></li>
      <li><p>Office Supplies</p></li>
      <li><p>Facilities Essentials - Appliances and Television</p></li>
    </ul>
  </div>
</div>
<footer>
	<div id="footer">
		<a href="<?php echo URL ?>contact#shaun"><h1 class="consHead">CONTACT US</h1>
