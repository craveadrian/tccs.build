<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hdLeft col fl">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
							<li <?php $this->helpers->isActiveMenu("construction"); ?>><a href="<?php echo URL ?>construction">CONSTRUCTION</a></li>
							<li <?php $this->helpers->isActiveMenu("consulting"); ?>><a href="<?php echo URL ?>consulting">CONSULTING</a></li>
							<li <?php $this->helpers->isActiveMenu("constructionphotos"); ?>><a href="<?php echo URL ?>constructionphotos">CONSTRUCTION PHOTOS</a></li>
							<li <?php $this->helpers->isActiveMenu("consultingphotos"); ?>><a href="<?php echo URL ?>consultingphotos">CONSULTING PHOTOS</a></li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
						</ul>
					</nav>
				</div>
				<div class="hdRight col fr">
					<p><i class="fa fa-phone"></i><span><?php $this->info(["phone","tel"]); ?><span></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner" class="landing">
			<div class="row">
				<a href="<?php echo URL ?>"><img src="public/images/common/mainLogo.png" alt="Logo"></a>
				<p class="tag">Building a Better Tomorrow</p>
			</div>
		</div>
	<?php endif; ?>
	<?php if($view == "construction" || $view == "constructionphotos"):?>
		<div id="banner" class="construction">
			<div class="row">
				<a href="<?php echo URL ?>"><img src="public/images/common/mainLogo.png" alt="Logo"></a>
				<p class="page">CONSTRUCTION</p>
				<p class="tag inner">Building a Better Tomorrow</p>
			</div>
		</div>
	<?php endif; ?>
	<?php if($view == "consulting" || $view == "consultingphotos"):?>
		<div id="banner" class="consulting">
			<div class="row">
				<a href="<?php echo URL ?>"><img src="public/images/common/mainLogo.png" alt="Logo"></a>
				<p class="page">MEDICAL CONSULTING</p>
				<p class="tag inner">Building a Better Tomorrow</p>
			</div>
		</div>
	<?php endif; ?>
