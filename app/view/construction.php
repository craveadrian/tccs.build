<div id="missionSection">
  <div class="row">
    <div class="msLeft inb">
      <section>
        <h1>OUR MISSION</h1>
        <p>At Thomas Construction we are led by President and founder Jeff Thomas. Our team has over 40 years of commercial construction experience and we have been serving the Carolina’s since 1984. As a team, we prefer to be hands on with our clients, utilizing our construction knowledge to make difficult decisions and helping them through the process. At TCCS we strive to be honest, hard-working and up-front with our clients allowing them to get the greatest return on their investment. </p>
        <!-- <a href="<?php echo URL ?>contact#content" class="button">GET STARTED</a> -->
      </section>
    </div>
    <div class="msRight inb">
      <img src="public/images/content/construction/img1.jpg" alt="table">
    </div>
  </div>
</div>
<div id="services2Section">
  <div class="row">
    <dl>
      <dt> <img src="public/images/content/construction/svc1.jpg" alt="Services Icons"> </dt>
      <dd>
        <h4>SMART/EFFICIENT</h4>
        <p>At Thomas Construction we work together with our clients to provide a highly professional level of detail to each project. Our goal is to provide detailed and effective advise to our clients on job optimization to minimize their investment and secure a great end product. Our successful projects don’t just happen overnight, they require precise planning, scheduling and daily evaluation by our project team to ensure that our successful final goal is accomplished.</p>
      </dd>
    </dl>
    <dl>
      <dt> <img src="public/images/content/construction/svc2.jpg" alt="Services Icons"> </dt>
      <dd>
        <h4>WE WORK TOGETHER <br> TO INNOVATE</h4>
        <p>At TCCS our focus is on relationships to help deliver the final product. Utilizing today’s latest technology to manage our projects and subcontractors, we are able to provide a superior product to the client. </p>
      </dd>
    </dl>
    <dl>
      <dt> <img src="public/images/content/construction/svc3.jpg" alt="Services Icons"> </dt>
      <dd>
        <h4>ETHICAL</h4>
        <p>Striving every day to foster an open environment with complete transparency among the project team and the client, we ensure that our client gets the maximum value for their investment. Our client’s goals are shared and we work together to allow them to flourish.</p>
        <p>At Thomas Construction and Consulting we work alongside our customers, subcontractors and vendors to provide the ultimate level of integrity, respect, and fairness to all parties involved.</p>
      </dd>
    </dl>
  </div>
  <a href="<?php echo URL ?>contact#content" class="btn">LEARN MORE</a>
</div>
<div id="services3Section">
  <div class="row">
    <h3>THOMAS CONSTRUCTION</h3>
    <h1>PROVIDES SERVICES FOR:</h1>
    <div class="flex-boxes">
      <div class="fbLeft fb">
        <dl>
          <dt> <img src="public/images/content/construction/svc2img1.jpg" alt="Services Image"> </dt>
          <dd>ESTIMATING</dd>
        </dl>
        <dl>
          <dt> <img src="public/images/content/construction/svc2img3.jpg" alt="Services Image"> </dt>
          <dd>PRE-CONSTRUCTION & PLANNING</dd>
        </dl>
      </div>
      <div class="fbMid fb">
        <dl>
          <dt> <img src="public/images/content/construction/svc2img2.jpg" alt="Services Image"> </dt>
          <dd>ALL CONSTRUCTION NEEDS</dd>
        </dl>
        <dl>
          <dt> <img src="public/images/content/construction/svc2img4.jpg" alt="Services Image"> </dt>
          <dd>CONSULTING</dd>
        </dl>
      </div>
      <div class="fbRight fb">
        <dl>
          <dt> <img src="public/images/content/construction/svc2img5.jpg" alt="Services Image"> </dt>
          <dd>CONSTRUCTION MANAGEMENT</dd>
        </dl>
      </div>
    </div>
  </div>
</div>
<footer>
	<div id="footer">
		<a href="<?php echo URL ?>contact#shaun"><h1>CONTACT US</h1>
