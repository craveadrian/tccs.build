<div id="wlcSection">
	<div class="row">
		<h1>WELCOME</h1>
		<p class="desc">Welcome to Thomas Construction and Consulting Services. Our Team has proudly served the Carolinas for over 20 years and we would love to serve your needs. We specialize in commercial construction and spatial engineering and redesign. Let us know how our team can help you achieve your goals!</p>
		<div class="wlcLeft col fl">
			<h2>Who Is Thomas Construction and Consulting Services?</h2>
			<p>Thomas Construction and Consulting Services is a two-part company that works seamlessly together to provide you, the client, an incredible product.</p>
		</div>
		<div class="wlcRight col fr">
			<div class="wlcRightTop">
				<img src="public/images/content/svcImg1.jpg" alt="Services 1" class="svcImg fl">
				<div class="wlcRightText fl">
					<h4>FULL SERVICE GENERAL CONTRACTOR</h4>
					<p>Thomas Construction and Consulting is a full service licensed general contractor that specializes in office, medical, retail, restaurant, build out, and ground up construction. We are licensed in both North and South Carolina. Our expertise will provide what you need to create a great space.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="wlcRightBot">
				<img src="public/images/content/svcImg2.jpg" alt="Services 2" class="svcImg fl">
				<div class="wlcRightText fl">
					<h4>REDESIGN AND OPTIMIZATION</h4>
					<p>Thomas Construction and Consulting (TCCS) also specializes in the redesign and optimization of your current office, medical and retail/restaurant space to create optimal efficiency and storage for your space. </p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="pageSection">
	<div class="row">
		<dl>
			<a href="<?php echo URL ?>construction#content">
				<dt> <img src="public/images/common/pageBg1.jpg" alt="construction"> </dt>
				<dd>CONSTRUCTION</dd>
			</a>
		</dl>
		<dl>
			<a href="<?php echo URL ?>consulting#content">
				<dt> <img src="public/images/common/pageBg2.jpg" alt="construction"> </dt>
				<dd>CONSULTING SERVICES</dd>
			</a>
		</dl>
		<div class="clearfix"></div>
	</div>
</div>
<footer>
	<div id="footer">
		<a href="<?php echo URL ?>contact#content"><h1>CONTACT US</h1>
