<div id="content">
	<div class="row">
		<div class="inner-contact">
			<h2 id="shaun">Consulting</h2>
			<h3>Call for all your Consulting needs</h3>
			<p>Our Consultant Shaun would love to hear from you about renovating your space. Email Shaun at: <span><?php $this->info(["email","mailto"]);?></span></p>
			<h3>TCCS</h3>
			<p><?php $this->info(["phone2","tel"]); ?></p>
			<h3>Hours</h3>
				<p id="hours">Open today <span class="color">9:00 am – 5:00 pm <i class="fa fa-angle-down"></i></span></p>
				<table id="tbHours">
					<tr>
						<td>Mon</td>
						<td>9:00 am -5:00 pm <span class="color"><i class="fa fa-angle-up"></i></span></td>
					</tr>
					<tr>
						<td>Tue</td>
						<td>9:00 am – 5:00 pm</td>
					</tr>
					<tr>
						<td>Wed</td>
						<td>9:00 am – 5:00 pm</td>
					</tr>
					<tr>
						<td>Thu</td>
						<td>9:00 am – 5:00 pm</td>
					</tr>
					<tr>
						<td>Fri</td>
						<td>9:00 am – 5:00 pm</td>
					</tr>
					<tr>
						<td>Sat</td>
						<td>Closed</td>
					</tr>
					<tr>
						<td>Sun</td>
						<td>Closed</td>
					</tr>
				</table>
				</ul>
				<!-- <button type="button" class="eshaun">EMAIL SHAUN</button> -->
				<form id="shaun-form" action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<h4>EMAIL SHAUN</h4>
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="Name:">
					</label>
					<!-- <label><span class="ctc-hide">Address</span>
						<input type="text" name="address" placeholder="Address:">
					</label> -->
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="Email:">
					</label>
					<!-- <label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone" placeholder="Phone:">
					</label> -->
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn" disabled>SEND</button>
					<!-- <p class="formCancel">CANCEL</p> -->
				</form>
				<h2>Construction</h2>
				<h3>We would love to hear from you!</h3>
				<p class="longText">Any questions regarding possible work, past jobs, or building consultation please email Jeff. <span><a href="mailto:Jeff.Thomas@tccs.build">Jeff.Thomas@tccs.build</a></span></p>
				<h3>Thomas Construction and Consulting Services</h3>
				<p>10129 Thomas Payne Cir, Charlotte, North Carolina 28277, United States</p>
				<p><?php $this->info(["phone","tel"]); ?></p>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<h2>CONTACT US</h2>
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="Name:">
					</label>
					<label><span class="ctc-hide">Address</span>
						<input type="text" name="address" placeholder="Address:">
					</label>
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="Email:">
					</label>
					<label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone" placeholder="Phone:">
					</label>
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn" disabled>SEND</button>
				</form>
		</div>
	</div>
</div>
<footer>
	<div id="footer">
		<h1>CONTACT US</h1>
